const normalizeString = (inString) => {
    return inString.toLowerCase().replace(/\s+/g, '');
}

export default normalizeString;