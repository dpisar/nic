import data from './data';
import convert from "xml-js";

const mapDataArray = (inData) => {
    const mappedData = inData.map((dataItem) => {
        return {
            id: dataItem.details.var[0]._text,
            name: dataItem.details.summary._text,
            author: dataItem.details.var[1]._text || "unknown",
            categoryId: dataItem.details.var[2]._text,
            category: dataItem.details.var[3]._text,
            imgSrc: dataItem.figure.img._attributes.src,
        }
    })

    return mappedData;
}

const parseData = () => {
    let exampleOfdata = [];

    try {
        const jsonString = convert.xml2json(data, {compact: true, spaces: 4});
        const rawData = JSON.parse(jsonString);
        exampleOfdata = mapDataArray(rawData.body.article)
    } catch (e) {
        console.info(e);
    }

    return exampleOfdata;
}

export default parseData;