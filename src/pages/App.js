import React, {Component} from 'react';
import parseData from "../helpers/parseData";
import SearchPanel from '../components/SearchPanel';
import BooksList from '../components/BooksList';
import Loader from '../components/Loader';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            filteredItems: [],
            showNoDataMessage: false,
        };
    }

    componentDidMount() {
        const data = parseData();

        this.setState({
            items: data,
            filteredItems: data,
        })

        this.handleData = this.handleData.bind(this);
    }

    handleData(filteredData) {
        if (filteredData.length > 0) {
            this.setState({
                filteredItems: filteredData,
            })
        } else {
            this.setState({
                showNoDataMessage: true,
            })
        }
    }

    render() {
        const {filteredItems = [], items, showNoDataMessage} = this.state;
        return (
            <div className="App scope-app">
                <div className="head-part">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <h1 className="h1">WorldCat.org: The World's Largest Library Catalog</h1>

                                <SearchPanel
                                    inData={items}
                                    handleData={this.handleData}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="content-part">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                {!showNoDataMessage &&
                                <React.Fragment>
                                    {filteredItems.length > 0 ? <BooksList items={filteredItems}/> : <Loader/>}
                                </React.Fragment>
                                }
                                {showNoDataMessage &&
                                <div className="alert alert-warning" role="alert">
                                    We didn't find any books.
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container footer-part">
                    <div className="container">
                        <div className="footer-part_text">Count of searched
                            items {filteredItems.length}/{items.length}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
