import React from 'react';
import PropTypes from 'prop-types';
import {Formik} from 'formik';
import normalizeString from "../helpers/normalizeString";

const SearchPanel = ({inData, handleData}) => {
    const prepareSearch = (dataFromForm) => {
        let exData = [];

        if (dataFromForm.searchText) {
            exData = inData.filter((item) => {
                if (['name', 'author', 'category'].includes(dataFromForm.searchCategory)) {
                    return normalizeString(dataFromForm.searchText) === normalizeString(item[dataFromForm.searchCategory]);
                }

                if (dataFromForm.searchCategory === 'author_name') {
                    return normalizeString(dataFromForm.searchText) === normalizeString(item["author"]) || normalizeString(dataFromForm.searchText) === normalizeString(item["name"])
                }

                if (dataFromForm.searchCategory === 'name_author_category') {
                    return normalizeString(dataFromForm.searchText) === normalizeString(item["author"]) || normalizeString(dataFromForm.searchText) === normalizeString(item["name"]) || normalizeString(dataFromForm.searchText) === normalizeString(item["category"])
                }
                return false;
            })
        }

        handleData(exData);
    }

    return (
        <React.Fragment>

            <Formik
                initialValues={{searchText: '', searchCategory: 'name', useRegex: false}}
                onSubmit={(values) => {
                    prepareSearch(values)
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                  }) => (
                    <form onSubmit={handleSubmit}>
                        <div style={{
                            fontSize: '0.75rem',
                            color: 'orange'
                        }}>{errors.searchText && touched.searchText && errors.searchText}</div>
                        <div className="search-box_wrapper">
                            <div className="form-group search-box_cell">
                                <label htmlFor="searchedTextInput">Text *</label>
                                <input
                                    id="searchedTextInput"
                                    type="text"
                                    name="searchText"
                                    className="form-control"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.searchText}
                                />

                            </div>

                            <div className="form-group search-box_cell">
                                <label htmlFor="searchCategory">Search in</label>
                                <select
                                    className="form-control"
                                    id="searchCategory"
                                    value={values.searchCategory}
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                >
                                    <option value="name">Name</option>
                                    <option value="author">Author</option>
                                    <option value="author_name">Author and name</option>
                                    <option value="category">Category</option>
                                    <option value="name_author_category">All</option>
                                </select>
                            </div>

                            <div className="form-group">
                                <div className="form-check search-box_cell search-box_regex">
                                    <input
                                        className="form-check-input"
                                        type="checkbox"
                                        id="useRegex"
                                        value={values.useRegex}
                                        onBlur={handleBlur}
                                        onChange={handleChange}

                                    />
                                    <label className="form-check-label" htmlFor="useRegex">
                                        Use regex
                                    </label>
                                </div>
                            </div>

                            <div className="search-box_cell search-box_submit">
                                <button
                                    className="btn btn-success"
                                    type="submit"
                                >
                                    Search
                                </button>
                            </div>

                        </div>
                    </form>
                )}
            </Formik>


        </React.Fragment>
    )
}

SearchPanel.propTypes = {
    inData: PropTypes.array,
    handleData: PropTypes.func
};

export default SearchPanel;