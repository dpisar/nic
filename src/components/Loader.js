import React from 'react';

const Loader = () => {
    return (
        <div className="text-center">
            <div className="spinner-border text-info" style={{marginTop: '2rem'}} role="status">
                <span className="sr-only" >Loading...</span>
            </div>
        </div>
    )
}

 export default Loader;