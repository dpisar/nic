import React from 'react';
import ListItem from './ListItem'
import PropTypes from "prop-types";

const BooksList = ({items}) => {
    return (
        <div className="books-list_wrapper">
            {
                items.map((oneItem) => {
                    return <ListItem key={oneItem.id} item={oneItem} />
                })
            }
        </div>
    )
}

BooksList.propTypes = {
    item: PropTypes.array,
};

export default BooksList;