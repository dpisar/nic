import React from "react";
import PropTypes from "prop-types";
import SearchPanel from "./SearchPanel";

const Item = ({item}) => {
    return (
        <div className="list-item_wrapper">
            <input className="item-state-input" type="checkbox" value="" id={`stateOwner${item.id}`}/>
            <div className="list-item_name">
                <label
                    className="h4 form-check-label"
                    htmlFor={`stateOwner${item.id}`}
                >
                    {item.name}
                </label>
            </div>

            <div className="list-item_params">
                <div className="list-item_params-inner">
                    <div className="list-item_params-text">
                        <div>
                            <span className="param-label">Autor:</span>
                            {item.author || 'Unknown'}
                        </div>
                        <div>
                            <span className="param-label">Category:</span>
                            {item.category}
                        </div>
                    </div>

                    <div className="list-item_params-image">
                        {item.imgSrc &&
                        <img
                            src={item.imgSrc}
                            alt={item.name}
                        />
                        }

                    </div>
                </div>
            </div>
        </div>
    )
}

Item.propTypes = {
    item: PropTypes.object,
};

export default Item;